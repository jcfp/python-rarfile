python-rarfile (4.2-3) unstable; urgency=medium

  * Control: remove nocheck marker from build-dep on python3-all.
  * Control, tests: add (build-)dep on 7zip-standalone, for 7zz.
  * Control: add a suggested dep on 7zip-standalone in the binary
    package, same as other supported archiver utils.
  * Patches: add 02_avoid_test_breakage.diff, to xfail all tests
    affected by an issue involving gnustep and unar.
  * Tests: allow all test runs to complete even in case of failures.

 -- Jeroen Ploemen <jcfp@debian.org>  Thu, 06 Mar 2025 07:19:49 +0000

python-rarfile (4.2-2) unstable; urgency=medium

  * Control: drop (build-)deps on python3-pycryptodome, unused.
  * Control: add build-depend and suggest on unrar-free, now that a
    recent enough version of that was packaged.
  * Rules: disable all tests that unrar-free cannot handle.
  * Tests: leverage the alternatives mechanism to run the upstream
    testsuite with both unrar-nonfree and unrar-free; for the latter,
    disable a large number of tests broken by its limited format
    support.
  * Bump Standards-Version to 4.7.0 (no further changes).

 -- Jeroen Ploemen <jcfp@debian.org>  Sat, 19 Oct 2024 06:47:17 +0000

python-rarfile (4.2-1) unstable; urgency=medium

  * New upstream version 4.2
  * Control: remove build-dep and suggest on p7zip. (Closes: #1063613)
  * Control: remove team from uploaders.
  * Control: point VCS links to private namespace.
  * Control: add build-dep on python3-cryptography, needed for tests.
  * Copyright: bump years.
  * Rules: add test_rar3_header_encryption to skipped-on-build tests.
  * Tests: move away from autopkgtest-pkg-pybuild in favour of custom
    d/tests, to enable running tests that use non-free unrar as part
    of the autopkgtest.
  * Control: add python3-cryptography as an alternative dep for python3-
    pycryptodome.

 -- Jeroen Ploemen <jcfp@debian.org>  Sun, 07 Apr 2024 07:48:27 +0000

python-rarfile (4.1-2) unstable; urgency=medium

  * Control: add all supported compression backends as suggested
    dependencies.

 -- Jeroen Ploemen <jcfp@debian.org>  Thu, 21 Sep 2023 09:40:21 +0000

python-rarfile (4.1-1) unstable; urgency=medium

  * New upstream version 4.1
  * Control: new maintainer. (Closes: #1050272)
  * Control: bump compat level to 13 (from 12).
  * Control: bump std-ver to 4.6.2 (from 4.4.1).
  * Control: update description.
  * Control: add build-dep on pybuild-plugin-pyproject.
  * Control: replace build-dep on dh-python with dh-sequence-python3.
  * Control: add build-deps on 7zip, libarchive-tools, p7zip, python3-
    cryptodome, python3-pytest, unar to support running the upstream
    testsuite on build.
  * Rules: exclude tests that require (non-free) unrar.
  * Control: remove dependency on ${shlibs:Depends}, pointless for an
    arch-all package.
  * Control: add dependency on python3-cryptodome to the binary pkg.
  * Docs: remove d/python3-rarfile.docs, duplicate of d/docs.
  * Watch: bump version to 4 (from 3; no further changes).
  * Copyright: bump upstream years, add myself for the packaging.
  * Copyright: fix typo in ISC license text.
  * Add upstream/metadata file.
  * Control: set Testsuite to autopkgtest-pkg-pybuild.
  * Control: set Rules-Requires-Root: no.
  * Patches: add 01 to avoid using the module itself as a pybuild
    testfile.

 -- Jeroen Ploemen <jcfp@debian.org>  Wed, 20 Sep 2023 09:15:39 +0000

python-rarfile (3.1-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

 -- Sandro Tosi <morph@debian.org>  Fri, 03 Jun 2022 23:31:13 -0400

python-rarfile (3.1-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Hugo Lefeuvre ]
  * New upstream release.
  * Update copyright years.
  * Run wrap-and-sort -a.

 -- Hugo Lefeuvre <hle@debian.org>  Wed, 30 Oct 2019 14:50:15 +0100

python-rarfile (3.0-2) unstable; urgency=medium

  * Team upload.
  * d/control: Set Vcs-* to salsa.debian.org
  * d/watch: Use https protocol
  * d/changelog: Remove trailing whitespaces
  * d/control: Remove trailing whitespaces
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support.
  * Bump debhelper compat level to 12.
  * Bump standards version to 4.4.0 (no changes).
  * Enable autopkgtest-pkg-python testsuite.

 -- Ondřej Nový <onovy@debian.org>  Sat, 10 Aug 2019 21:29:17 +0200

python-rarfile (3.0-1) unstable; urgency=medium

  * New upstream release.
  * Bump compat level from 9 to 10.
  * debian/control:
    - Bump Standards to 4.1.0.
    - Update description to match new upstream release.
    - Depend on debhelper >= 10 instead of >= 9.
    - Depend exclusively on libarchive-tools instead of bsdtar and
      unrar-free (Closes: #797730).
  * Update copyright years.

 -- Hugo Lefeuvre <hle@debian.org>  Thu, 05 Oct 2017 10:41:21 +0200

python-rarfile (2.8-1) unstable; urgency=low

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

  [ Hugo Lefeuvre ]
  * New upstream release.
  * debian/control:
    - Fix unnecessary versioned dependency python-all in the Build-Depends
      field.
    - Fix unnecessary versioned dependency bsdtar in the Depends field.
    - Bump Standards-Version to 3.9.8.
    - Update synopsis for both python2 and python3 packages to remove
      redundant word 'Python'.
  * debian/copyright:
    - Update Format field to match DEP5 recommendations.
    - Update copyright years.
  * debian/control, debian/copyright:
    - Update Hugo Lefeuvre's e-mail address.

 -- Hugo Lefeuvre <hle@debian.org>  Sat, 18 Jun 2016 21:45:36 +0200

python-rarfile (2.7-2) unstable; urgency=low

  * Upload to unstable.
  * debian/compat:
    - Bump compat level to 9.
  * debian/control:
    - Bump debhelper's minimal version to 9.
  * debian/watch:
    - Use pypi.debian.net instead of pypi.python.org, following
      DebianWiki's recommendation.
  * debian/copyright:
    - Update copyright years.
  * Delete README.Debian and python3-rarfile.README.Debian (no more
    relevant due to last upstream release).

 -- Hugo Lefeuvre <hugo6390@orange.fr>  Fri, 10 Apr 2015 23:08:35 +0200

python-rarfile (2.7-1) experimental; urgency=low

  * New upstream release
  * debian/control:
    - Bump Standards-Version to 3.9.6.
    - Update python-rarfile's and python-rarfile3's synopsis to
      fix capitalization errors.

 -- Hugo Lefeuvre <hugo6390@orange.fr>  Sun, 14 Dec 2014 12:02:25 +0100

python-rarfile (2.6-1) unstable; urgency=low

  * Initial release (Closes: #680085)

 -- Hugo Lefeuvre <hugo6390@orange.fr>  Mon, 09 Jun 2014 18:59:40 +0200
